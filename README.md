# Count frequency for words

Find a simple util called `countWordFrequency` in `src/countWordFrequency.js` that takes in a multiline string, cleans it up a bit and counts the word frequencies.


## Setup 
1. `git clone https://gitlab.com/ksiebert1/count-words.git`
2. Go into the project folder
3. Run `npm install`
4. Run `yarn start` to see the UI at [http://localhost:3000](http://localhost:3000)
4. Check tests with `yarn test`


This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Next steps
Check for a couple more edge cases like `it's` when we know how to count them
