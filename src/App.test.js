import countWordFrequency from "./countWordFrequency"

test('count util should sum up frequency duplicate', () => {
  expect(countWordFrequency(`word word`)).toMatchObject({word: 2});
})

test('count util should sum up frequency independent of case', () => {
  expect(countWordFrequency(`Word word`)).toMatchObject({word: 2});
})

test('count util should handle special characters', () => {
  expect(countWordFrequency(`USA—that`)).toMatchObject({usa: 1, that: 1});
})

test('count util should check param type', () => {
  expect(countWordFrequency(2)).toMatchObject({});
})