
import './App.css';
import WordMachine from './WordMachine.js'

const App = () => {
  return (
    <div className="App">
      <WordMachine/>
    </div>
  );
}

export default App;
