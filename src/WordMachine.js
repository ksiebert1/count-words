import allTheText from "./text.js";
import countWordFrequency from "./countWordFrequency";

console.log(allTheText)

const WordMachine = () => {

    const wordCountLookup = countWordFrequency(allTheText)
        
    return (Object.keys(wordCountLookup).map(key =>
      (<div>
          {`${key} ${wordCountLookup[key]}`}
      </div>)
     )
    );
  }
  
export default WordMachine;