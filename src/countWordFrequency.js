/* 
Takes in a multi line string as a parameter
    multiLineString: string

RETURNS object of keys and their count as a value
*/
const countWordFrequency = (multiLineString) => {

    if (typeof multiLineString !== 'string' || !multiLineString instanceof String){
     console.info("You need to pass in a valid string")
     return {};
    }

    const lookUpObject = {}
    const textWithoutPunctuation = multiLineString.replace(/[.,/#!$%^&*;:{}=\-_`"“~()]/g,"").toLowerCase();
    const textWithoutHyphen = textWithoutPunctuation.replace(/[—]/g," ")
    const wordArray = textWithoutHyphen.split(" ")
    
    //Create array of unique words
    const setOfWords = new Set(wordArray)
    
    //Init Lookup table
    setOfWords.forEach(uniqueWord => {
        lookUpObject[uniqueWord] = 0
    })
    //Do actual frequency count
    wordArray.forEach(word => {
        lookUpObject[word] ++
    })
    return lookUpObject
} 

export default countWordFrequency